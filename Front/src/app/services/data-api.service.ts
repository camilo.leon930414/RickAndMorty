import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DataApiService {

    constructor(private http: HttpClient) { }
    /* metodo buscar data */
    getData(url) {
        const urlApi = url;
        return this.http.get<any>(urlApi);
    }
    /* metodo buscar con string */
    getByString(char: any,url: string) {
        let urlApi = url+char;
        if(!char){
            urlApi = url;
        }
        return this.http.get<any>(urlApi);
    }
     /* metodo buscar con Numerico */
    getByNumber(page: any,url: string) {
        const urlApi = url+page;
        return this.http.get<any>(urlApi);
    }
}




