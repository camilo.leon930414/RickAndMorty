import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GetCharactersComponent } from './components/get-characters/get-characters.component';
import { DetailCharComponent } from './components/detail-char/detail-char.component';
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './components/home/home.component';
import { SearchCharactersComponent } from './components/search-characters/search-characters.component';
import { GetChaptersComponent } from './components/get-chapters/get-chapters.component';
/* loader */
import { LoaderComponent } from './components/loader/loader.component'
import { LoaderService } from './services/guards/loader.service';
import  {LoaderInterceptorService } from './services/guards/loader-interceptor.service';
import { DetailEpisodeComponent } from './components/detail-episode/detail-episode.component';

/* material */
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRadioModule }  from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';

/*  */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    GetCharactersComponent,
    DetailCharComponent,
    NavComponent,
    HomeComponent,
    SearchCharactersComponent,
    GetChaptersComponent,
    LoaderComponent,
    DetailEpisodeComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    DragDropModule,
    MatBottomSheetModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSortModule,
    MatTableModule,
    MatPaginatorModule,
    MatTooltipModule, 
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,  
    MatCheckboxModule, 
    BrowserAnimationsModule
  ],
  providers: [
    LoaderService,
    {provide:HTTP_INTERCEPTORS,useClass:LoaderInterceptorService,multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
