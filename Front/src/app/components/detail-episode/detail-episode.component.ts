import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataApiService } from '../../services/data-api.service';
import { Location } from '@angular/common';

@Component({
    selector: 'app-detail-episode',
    templateUrl: './detail-episode.component.html',
    styleUrls: ['./detail-episode.component.css']
})
export class DetailEpisodeComponent implements OnInit {

    episode: any;
    char: string;
    allCharacters: any = [];
    constructor(private dataApi: DataApiService, private route: ActivatedRoute, private location: Location) { }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        this.getDetailChapter(parseInt(id));
    }

    getDetailChapter(id: number) {
        let UrlApi ='https://rickandmortyapi.com/api/episode/'
        this.dataApi.getByNumber(id,UrlApi).subscribe((response) => {
            this.episode = response;
        },
            (error) => { console.error(error); }
        );
    }

    backClicked() {
        this.location.back();
    }

    getChapterByCaracter() {
        this.allCharacters = [];
        let UrlApi = false;
        this.episode['characters'].forEach(url => {
            this.dataApi.getByString(UrlApi,url).subscribe((response) => {
                this.allCharacters.push({
                    id: response['id'],
                    name: response['name'],
                    imagen: response['image'],
                    status: response['status'],
                    species: response['species'],
                    gender :response['gender']
                });
            },
                (error) => { console.error(error); }
            );
        });
    }
}
