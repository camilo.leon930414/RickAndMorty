import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataApiService } from '../../services/data-api.service';

@Component({
  selector: 'app-search-characters',
  templateUrl: './search-characters.component.html',
  styleUrls: ['./search-characters.component.css']
})
export class SearchCharactersComponent implements OnInit {

  result: any[];
  mensajeError: '';
  constructor(private dataApi: DataApiService) { }

  ngOnInit(): void {
  }

  searchChar(formSearch: NgForm) {
    let UrlApi ='https://rickandmortyapi.com/api/character/?name=';
    const searchCharacter = formSearch.value.nameChar;
    this.dataApi.getByString(searchCharacter,UrlApi).subscribe((response => console.log(response)));
    this.dataApi.getByString(searchCharacter,UrlApi).subscribe((response) => console.log('response ' + response['info'].count));
    this.dataApi.getByString(searchCharacter,UrlApi).subscribe((response) => {
      this.result = response;
      this.mensajeError = '';
    },
      (error) => {
        console.error('Este es el error ' + error.ok),
          this.mensajeError = error.status,

          console.log('mensaje error: ' + this.mensajeError);
        this.result = [];
      }
    );
    return this.mensajeError;
  }

  getPage(urlApi) {
    let UrlApi ='https://rickandmortyapi.com/api/character/?page=';
    if (urlApi === null || urlApi === '') {
      //
    } else {
      const page = urlApi.substring(48, urlApi.lenght);
      if (page === null || page === '') {
        this.dataApi.getByNumber(1,UrlApi).subscribe((response) => {
          this.result = response;
        },
          (error) => { console.error(error); }
        );
      } else {
        console.log('Número de página: ' + page);
        this.dataApi.getByNumber(page,UrlApi).subscribe((response) => {
          this.result = response;
        },
          (error) => { console.error(error); }
        );
        this.dataApi.getByNumber(page,UrlApi).subscribe((characters) => console.log(this.result));
        this.mensajeError = '';
      }
    }
  }

  goToPage(formIrPagina: NgForm) {
    let UrlApi ='https://rickandmortyapi.com/api/character/?page=';
    const pagina = formIrPagina.value.pagina;
    this.dataApi.getByNumber(pagina,UrlApi).subscribe((characters) => console.log(this.result));

    this.dataApi.getByNumber(pagina,UrlApi).subscribe((response) => {
      this.result = response;
      this.mensajeError = '';
    },
      (error) => {
        console.error('Este es el error ' + error.ok),
          this.mensajeError = error.status,

          console.log('kkmensaje error: ' + this.mensajeError);

      }
    );
    return this.mensajeError;
  }
}
