import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataApiService } from '../../services/data-api.service';
import { Location } from '@angular/common';

@Component({
    selector: 'app-detail-char',
    templateUrl: './detail-char.component.html',
    styleUrls: ['./detail-char.component.css']
})
export class DetailCharComponent implements OnInit {

    character: any;
    char: string;
    chapters:any = [];
    constructor(private dataApi: DataApiService, private route: ActivatedRoute, private location: Location) { }

    ngOnInit(): void {
        const char = this.route.snapshot.paramMap.get('char');
        this.getDetailChar(char);
        
    }

    getDetailChar(char: string) {
        let UrlApi = 'https://rickandmortyapi.com/api/character/';
        this.dataApi.getByString(char,UrlApi).subscribe((response) => {
            this.character = response;
        },
        (error) => { console.error(error); }
        );
    }

    backClicked() {
        this.location.back();
    }

    getChapterByCaracter(){
        let UrlApi = false;
        this.character['episode'] .forEach(url => {
            this.dataApi.getByString(UrlApi,url).subscribe((response) => {
                let date = new Date(response['created']);
                let day = date.getDate()
                let month = date.getMonth() + 1
                let year = date.getFullYear()
                let date_created = "";
                if(month < 10){
                    date_created =`${day}-0${month}-${year}`;
                  }else{
                    date_created =`${day}-${month}-${year}`;
                  }
                this.chapters.push({
                    name:response['name'],
                    date:response['air_date'],
                    created:date_created,
                    episode:response['episode']
                });
            },
                (error) => { console.error(error); }
            );
        });
    }

}
