import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataApiService } from '../../services/data-api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-get-characters',
    templateUrl: './get-characters.component.html',
    styleUrls: ['./get-characters.component.css']
})
export class GetCharactersComponent implements OnInit {
    mensajeError: string;
    characters: any[] = ['results'];
    urlApi: string;
    paginaNow = 1;
    constructor(private dataApi: DataApiService, private route: ActivatedRoute) { }


    ngOnInit(): void {
        this.getCharacters();
    }

    getCharacters() {
        let UrlApi ='https://rickandmortyapi.com/api/character/';
        this.dataApi.getData(UrlApi).subscribe((response) => {
            this.characters = response;
        },
            (error) => { console.error(error); }
        );
    }

    getPage(urlApi) {
        let UrlApi = 'https://rickandmortyapi.com/api/character/?page=';
        if (urlApi === null || urlApi === '') {
            //
        } else {
            const page = urlApi.substring(48, urlApi.lenght); /* trae solo el numero de la pagina de toda la url */
            if (page === null || page === '') {
                this.dataApi.getByNumber(1,UrlApi).subscribe((response) => {
                    this.characters = response;
                },
                    (error) => { console.error(error); }
                );
            } else {
                this.paginaNow = page;
                this.dataApi.getByNumber(page,UrlApi).subscribe((response) => {
                    this.characters = response;
                },
                    (error) => { console.error(error); }
                );
                this.mensajeError = '';
            }
        }
    }

    goToPage(formIrPagina: NgForm) {
        let UrlApi = 'https://rickandmortyapi.com/api/character/?page=';
        const pagina = formIrPagina.value.pagina;
        this.paginaNow = pagina;
        this.dataApi.getByNumber(pagina,UrlApi).subscribe((response) => {
            this.characters = response;
            this.mensajeError = '';
        },
            (error) => {
                console.error('Este es el error ' + error.ok),
                    this.mensajeError = error.status,

                    console.log('mensaje error: ' + this.mensajeError);
            }
        );
        return this.mensajeError;
    }
}
