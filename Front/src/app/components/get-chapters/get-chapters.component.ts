import { Component, OnInit,ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataApiService } from '../../services/data-api.service';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

export interface chapters {
    id:number;
    episode: string;
    name: string;
    created: string;
}

@Component({
    selector: 'app-get-chapters',
    templateUrl: './get-chapters.component.html',
    styleUrls: ['./get-chapters.component.css']
})
export class GetChaptersComponent implements OnInit {

    mensajeError: string;
    chapters: any[] = ['results'];
    urlApi: string;
    paginaNow = 1;
    /* tabla */
    public dataSource = new MatTableDataSource<chapters>();
    public displayedColumns = ['ID', 'NOMBRE', 'EPISODIO', 'CREADO','VER'];

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private dataApi: DataApiService, private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.getAllChapters();

        /* filtrado */
        this.dataSource.filterPredicate = (data: any, filter: string) => {
            return data['name'].toLowerCase().includes(filter.toLowerCase());
        };
    }

    /* metodos de la tabla material */
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    /* aplicar filtro */
    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue;
    }

    getAllChapters() {
        this.dataSource.data = [];
        let UrlApi ='https://rickandmortyapi.com/api/episode';
        this.dataApi.getData(UrlApi).subscribe((response) => {
            let data: chapters[] = response['results'];
            this.chapters = response;
            this.dataSource.data = data;
        },
            (error) => { console.error(error); }
        );
    }

    getPage(urlApi) {
        let UrlApi ="https://rickandmortyapi.com/api/episode/?page=";
        if (urlApi === null || urlApi === '') {
            //
        } else {
            const page = urlApi.substring(46, urlApi.lenght);/* trae solo el numero de la pagina de toda la url */
            if (page === null || page === '') {
                this.dataApi.getByNumber(1,UrlApi).subscribe((response) => {
                    this.chapters = response;
                },
                    (error) => { console.error(error); }
                );
            } else {
                this.paginaNow = page;
                this.dataApi.getByNumber(page,UrlApi).subscribe((response) => {
                    this.chapters = response;
                },
                    (error) => { console.error(error); }
                );
                this.mensajeError = '';
            }
        }
    }

    goToPage(formIrPagina: NgForm) {
        let UrlApi ="https://rickandmortyapi.com/api/episode/?page=";
        const pagina = formIrPagina.value.pagina;
        this.paginaNow = pagina;
        this.dataApi.getByNumber(pagina,UrlApi).subscribe((response) => {
            this.chapters = response;
            this.mensajeError = '';
        },
            (error) => {
                console.error('Este es el error ' + error.ok),
                    this.mensajeError = error.status,
                    console.log('mensaje error: ' + this.mensajeError);
            }
        );
        return this.mensajeError;
    }

}
