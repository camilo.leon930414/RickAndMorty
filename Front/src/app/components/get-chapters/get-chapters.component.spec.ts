import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetChaptersComponent } from './get-chapters.component';

describe('GetChaptersComponent', () => {
  let component: GetChaptersComponent;
  let fixture: ComponentFixture<GetChaptersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetChaptersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetChaptersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
